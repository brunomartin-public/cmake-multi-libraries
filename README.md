CMake multi libs
===

This project try to demonstrate feasability of dependent subprojects for build, test and packaging :
- LibB depends on libA,
- each one may be build, tested and packaged separately in each subdirectory and
- global build, tested and packaged on root directory is also possible to develop and test libraries and their interaction.
- To build only libA, type the following command in repository directory:
```
mkdir -p lib_a/build && cmake -S lib_a -B lib_a/build && \
  cmake --build lib_a/build && ctest --test-dir lib_a/build && \
  cpack -B lib_a/build --config lib_a/build/CPackConfig.cmake
```

- To build only libB with required from libA (tests have to be filtered unless libATest will fail be cause not built), type the following command in repository directory:
```
mkdir -p lib_b/build && cmake -S lib_b -B lib_b/build && \
  cmake --build lib_b/build && ctest --test-dir lib_b/build -R LibB && \
  cpack -B lib_b/build --config lib_b/build/CPackConfig.cmake
```

- To build all, type the following command in repository directory:
```
mkdir -p build && cmake -S . -B build && \
  cmake --build build && ctest --test-dir build && \
  cpack -B build --config build/CPackConfig.cmake
```

- To clean all:
```
rm -Rf lib_a/build lib_b/build build
```
