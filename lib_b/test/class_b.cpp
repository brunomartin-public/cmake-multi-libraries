#include <cassert>
#include <exception>

#include <lib_b/class_b.h>

#define ASSERT(X) if(!(X)) return -1;

int main(int argc, char* argv[]) {
  ClassB class_b;
  ASSERT(class_b.Add(2, 3) == 5);
  ASSERT(class_b.Multiply(2, 3) == 6);

  ASSERT(class_b.Substract(2, 3) == -1);
  ASSERT(class_b.Divide(4, 2) == 2);
  ASSERT(class_b.Divide(3, 2) == 1);

  try {
    class_b.Divide(2, 0);
    ASSERT(false);
  } catch(Exception& e) {}

  return 0;
}
