#pragma once

#include <exception>
#include <string>

#include <lib_a/class_a.h>

class Exception : std::exception {
public:
  Exception(const std::string& message);
  virtual const char* what() const noexcept;

private:
  std::string message_;
};

class ClassB : public ClassA {
public:
  int Multiply(int x, int y);
  int Divide(int x, int y);

};
