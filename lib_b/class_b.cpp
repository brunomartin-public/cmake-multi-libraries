#include "lib_b/class_b.h"

#include <exception>

Exception::Exception(const std::string& message) : message_(message) {

}

const char* Exception::what() const noexcept {
  return message_.c_str();
}

int ClassB::Multiply(int x, int y) {
  return x * y;
}

int ClassB::Divide(int x, int y) {

  if(y == 0.) {
    throw Exception("Cannot divide by zero");
  }

  return x / y;
}
