#include <cassert>
#include <iostream>

#include <lib_a/class_a.h>

#define ASSERT(X) if(!(X)) return -1;

int main(int argc, char* argv[]) {
  ClassA class_a;
  ASSERT(class_a.Add(2, 3) == 5);
  ASSERT(class_a.Substract(2, 3) == -1);

  return 0;
}
